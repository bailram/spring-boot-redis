package com.bailram.springbootredis.controller;

import com.bailram.springbootredis.entity.Product;
import com.bailram.springbootredis.repository.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class RedisController {
    @Autowired
    private ProductDao productDao;

    @PostMapping("/save")
    public Product save(@RequestBody Product product) {
        return productDao.save(product);
    }

    @PostMapping
    public List<Product> getAll() {
        return productDao.findAll();
    }

    @PostMapping("/{id}")
    public Product findById(@PathVariable int id) {
        return productDao.findById(id);
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable int id) {
        return productDao.deleteProduct(id);
    }

}
